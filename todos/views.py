from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.
def todo_list_list(request):
    todolists = TodoList.objects.all()
    context = {
        "todo_lists": todolists,
    }
    return render(request, "todos/todo_list_list.html", context)


def todo_list_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todolist,
    }
    return render(request, "todos/todo_list_detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/todo_list_create.html", context)


def todo_list_update(request, id):
    edit = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=edit)
        if form.is_valid():
            edit = form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=edit)
    context = {
        "form": form,
    }
    return render(request, "todos/todo_list_update.html", context)


def todo_list_delete(request, id):
    this_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        this_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/todo_list_delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
        else:
            form = TodoItemForm()
        context = {
            "form": form,
        }
        return render(request, "todos/todo_item_create.html", context)
